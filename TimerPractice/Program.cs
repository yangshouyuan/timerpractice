﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TimerPractice
{
    class Program
    {
        static int time = 0;
        static Timer timer;
        static long num;
        static void Main()
        {
            Console.WriteLine("Enter time interval in seconds:");
            if (long.TryParse(Console.ReadLine(), out num))
            {
                timer = new Timer(num * 1000);
                timer.Elapsed += timer_Elapsed;
                timer.Start();
                Console.ReadKey(true);
            }
            else
            {
                Console.WriteLine("Invalid input value!");
                Main();
            }
        }

        static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("{0} seconds now", time += Convert.ToInt32(timer.Interval/1000));
        }
    }
}
